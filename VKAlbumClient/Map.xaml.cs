﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;

namespace VKAlbumClient
{
    public sealed partial class Map : Page
    {
        static public Map instance { get; private set; }

        public Map()
        {
            this.InitializeComponent();
            instance = this;
            myMap.MapServiceToken = "erisDyRvA0Z8JJx2aPlS~XPbkcrHsDjVeJmbyXgtElQ~AtulC7fIEUJNOvxC5NOMPJuciZZh99NFDiyP_R-DvcYsgjb2D6D9TfwHK2uURbIC";
            myMap.Loaded += MyMap_Loaded;
        }

        private async void MyMap_Loaded(object sender, RoutedEventArgs e)
        {
            progress.IsEnabled = true;
            progress.Visibility = Windows.UI.Xaml.Visibility.Visible;

            Geolocator geo = new Geolocator();
            Geoposition pos = await geo.GetGeopositionAsync(); // get the raw geoposition data
            double lat = pos.Coordinate.Point.Position.Latitude; // current latitude
            double longt = pos.Coordinate.Point.Position.Longitude; // current longitude

            myMap.Center =
               new Geopoint(new BasicGeoposition()
               {
                   Latitude = lat,
                   Longitude = longt
               });
            myMap.ZoomLevel = 14;

            string strlat = lat.ToString().Replace(",", ".");
            string strlong = longt.ToString().Replace(",", ".");

            VKAPI.instance.GetMapPhoto(strlat, strlong, Finished);
        }


        public  void Finished()
        {
            myMap.DataContext = VKAPI.instance.MapImages;
        }


        public void HideProgress()
        {
            progress.IsEnabled = false;
            progress.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private void mapItemButton_Click(object sender, RoutedEventArgs e)
        {
   
                Button button = (Button)sender;
                Debug.WriteLine(button.DataContext);
                ImageInfo info = (ImageInfo)button.DataContext;
                this.Frame.Navigate(
                            typeof(ImageViewer),
                            info,
                            new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
   
        }
    }
}
