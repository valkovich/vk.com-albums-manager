﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;


namespace VKAlbumClient
{
    public sealed partial class Albums : Page
    {
        

        public Albums()
        {
            this.InitializeComponent();

            progress.IsEnabled = true;
            progress.Visibility = Windows.UI.Xaml.Visibility.Visible;
            SetAlbums();
        }

        public GridView albumsGrid;

        private void AlbumsGrid_Loaded(object sender, RoutedEventArgs e)
        {
            albumsGrid = (GridView)sender;
        }

        void SetAlbums()
        {
            AlbumsGrid.DataContext = VKAPI.instance.AlbumsPreview;
        }

        private void AlbumsList_ItemClick(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                AlbumsInfo Purchase_Invoice = (AlbumsInfo)albumsGrid.SelectedItems[0];

                this.Frame.Navigate(
                            typeof(AlbumViewer),
                            Purchase_Invoice,
                            new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
            }
            catch
            {
                Debug.WriteLine("Something happened with  private void AlbumsList_ItemClick(object sender, SelectionChangedEventArgs e)");
            }
        }

        private async void NewAlbumButton_Click(object sender, RoutedEventArgs e)
        {
            var result = await createAlbumDialog.ShowAsync();
        }

        private void cancelCreateAlbum_Click(object sender, RoutedEventArgs e)
        {
            createAlbumDialog.Hide();
            albumName.Text = "";
            albumDescription.Text = "";
        }

        private void createAlbum_Click(object sender, RoutedEventArgs e)
        {
            if (albumName.Text.Length >= 2)
            {
                string name = albumName.Text;
                string description = albumDescription.Text;
                int privacyID = albumsPrivacy.SelectedIndex;


                string privacy = String.Empty;

                switch(privacyID)
                {
                    case 0 : privacy = "0"; break;
                    case 1: privacy = "1"; break;
                    case 2: privacy = "3"; break;
                    default: privacy = "3"; break;
                }
                VKAPI.instance.CreateAlbum(name, description, privacy, AlbumCreationCallback);

                createAlbumDialog.Hide();
                albumName.Text = "";
                albumDescription.Text = "";
                
            }
            else
            {
                albumName.Text = "Название";
            }
        }

        public void AlbumCreationCallback(string json)
        {
            VKAPI.instance.GetAlbumsCount();
            Debug.WriteLine(json);
        }
    }


}
