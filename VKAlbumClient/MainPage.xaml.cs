﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace VKAlbumClient
{
    public sealed partial class MainPage : Page
    {
        private ApplicationWindow rootPage;

        static public MainPage instance { get; private set; }

        Image avatar;
        TextBlock userName;
        private GridView allPhotosPreviewGrid;

        public ObservableCollection<ImageInfo> allPhotosPreview;


        public MainPage()
        {
            this.InitializeComponent();
            instance = this;
            allPhotosPreview = VKAPI.instance.PreviewImages;
            SetAllAlbumsPrevies();
            SetMostPopular();
            SetMostCommentable();
            SetAllPhotosCount();

            if (allPhotosPreview != null)
            {
                HubMainPage.DataContext = allPhotosPreview;
                ClosePreviewProgress();
            }
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {        
            CloseAvatarProgress();
        }


        private void SetMinimumSize_Click(object sender, RoutedEventArgs e)
        {
            var view = ApplicationView.GetForCurrentView();
        }

   
  
        public void SetName()
        {
            if (userName != null)
                userName.Text = VKAPI.user.name;
        }

        public void SetAvatar()
        {
            if (avatar != null)
            {
                MainPage.instance.CloseAvatarProgress();
                avatar.Source = new BitmapImage(new Uri(VKAPI.user.photoUri));
            }
        }

        public void SetAllPhotosCount()
        {
            try
            {
                photosCount.Text = "Последние фотографии " + VKAPI.instance.photosCount.ToString();
            }
            catch
            {
                photosCount.Text = "Последние фотографии...";
            }
        }

        public void SetAllPhotosPreview()
        {
        }

        public void SetMostPopular()
        {
            mostPopular.DataContext = VKAPI.instance.LikebleSort;
        }

        public void SetMostCommentable()
        {
            mostCommentable.DataContext = VKAPI.instance.ImagesWithComments;
        }

        public void SetAllAlbumsPrevies()
        {
            if (allPhotosPreviewGrid != null)
            allPhotosPreviewGrid.DataContext = VKAPI.instance.PreviewImages;
            AlbumsHub.DataContext = VKAPI.instance.AlbumsPreview;
        }

        private void PreviewItemClick(object sender, SelectionChangedEventArgs e)
        {
            ImageInfo Purchase_Invoice = (ImageInfo)allPhotosPreviewGrid.SelectedItems[0];

            this.Frame.Navigate(
                        typeof(ImageViewer),
                        Purchase_Invoice,
                        new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
        }



        #region Подвязка котролов
        private void AvatarReady(object sender, RoutedEventArgs e)
        {
            avatar = (Image)sender;
            SetAvatar();
        }

        private void UserName_Loaded(object sender, RoutedEventArgs e)
        {
            userName = (TextBlock)sender;
            userName.Text = VKAPI.user.name;
        }

        private void PhotosCount_Loaded(object sender, RoutedEventArgs e)
        {
            photosCount = (TextBlock)sender;
        }

        private void previewGridLoaded_Loaded(object sender, RoutedEventArgs e)
        {
            allPhotosPreviewGrid = (GridView)sender;
        }
        #endregion

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Avatar_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {

        }

        private void avatarList_ItemClick(object sender, SelectionChangedEventArgs e)
        {
            Debug.WriteLine("CLICK " + VKAPI.instance.avatarInfo.path + " " + VKAPI.instance.avatarInfo.pid);

            this.Frame.Navigate(
                       typeof(ImageViewer),
                       VKAPI.instance.avatarInfo,
                       new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());

            Debug.WriteLine("CLICK " + VKAPI.instance.avatarInfo.path + " " + VKAPI.instance.avatarInfo.pid);

        }

        private void AlbumsHub_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                AlbumsInfo Purchase_Invoice = (AlbumsInfo)AlbumsHub.SelectedItems[0];

                this.Frame.Navigate(
                            typeof(AlbumViewer),
                            Purchase_Invoice,
                            new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
            }
            catch
            {
                Debug.WriteLine("Something happened private void AlbumsHub_SelectionChanged(object sender, SelectionChangedEventArgs e)");
            }

        }

        private void myMap_Loaded(object sender, RoutedEventArgs e)
        {
            //myMap.Center =
            // new Geopoint(new BasicGeoposition()
            // {
            //       //Geopoint for Seattle 
            //       Latitude = 47.604,
            //     Longitude = -122.329
            // });
            //myMap.ZoomLevel = 17;
        }

        private void mostPopular_Loaded(object sender, RoutedEventArgs e)
        {
            SetMostPopular();
        }

        private void mostPopular_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ImageInfo Purchase_Invoice = (ImageInfo)mostPopular.SelectedItems[0];

                this.Frame.Navigate(
                            typeof(ImageViewer),
                            Purchase_Invoice,
                            new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
            }
            catch
            {
                Debug.WriteLine("Something happened  private void mostPopular_SelectionChanged(object sender, SelectionChangedEventArgs e)");
            }
        }

        private void mostCommentable_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void mostCommentable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ImageInfo Purchase_Invoice = (ImageInfo)mostCommentable.SelectedItems[0];

                this.Frame.Navigate(
                            typeof(ImageViewer),
                            Purchase_Invoice,
                            new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
            }
            catch
            {
                Debug.WriteLine("Something happened  private void mostCommentable_SelectionChanged(object sender, SelectionChangedEventArgs e)");
            }
        }

        public void CloseAvatarProgress()
        {
            progressAvatar.IsActive = false;
            progressAvatar.IsEnabled = false;
            progressAvatar.Visibility = Visibility.Collapsed;
        }


        public void ClosePreviewProgress()
        {
            progressPreviewr.IsActive = false;
            progressPreviewr.IsEnabled = false;
            progressPreviewr.Visibility = Visibility.Collapsed;
        }

 
    }


}
