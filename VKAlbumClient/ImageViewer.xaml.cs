﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace VKAlbumClient
{


    public sealed partial class ImageViewer : Page
    {
        static public ImageViewer instance { get; private set; }

        private ImageInfo currentInfo;
        private int currentIndex = 0;
        private List<ImageInfo> currentAlbum;
        private bool firstOpen = true;

        public ImageViewer()
        {
            this.InitializeComponent();
            instance = this;

        }

        void ShowProgress()
        {
            progressRing.IsActive = true;
        }

        void HideProgress()
        {
            progressRing.IsActive = false;
        }

        ImageInfo startInfo;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           ImageInfo info = (ImageInfo)e.Parameter;
           startInfo = info;
           DownloadImage(info);
           base.OnNavigatedTo(e);
        }

        void DownloadImage(ImageInfo info)
        {
            ShowProgress();
            VKAPI.instance.DownloadFullSizeImage(info,Downloaded);
        }

        public void Downloaded(ImageRequest request, ImageInfo info)
        {
            HideProgress();
            currentInfo = info;
            fio.Content = info.fio;

            if (request.isSuccess)
            {
                currentInfo = info;
                Blur();
            }


            if (info.owner_id == VKAPI.instance.userId.ToString())
            {
                Settings.IsEnabled = true;
            }
            else
            {
                Settings.IsEnabled = false;
            }
            RequestAlbum();
        }

        void RequestAlbum()
        {
            AlbumsInfo info = new AlbumsInfo(startInfo, "");
            info.owner_id = startInfo.owner_id;
            info.aid = startInfo.aid;
            VKAPI.instance.DownloadAlbumFormCurrentImage(info, AlbumDownloaded);
        }

        public void AlbumDownloaded(List<ImageInfo> album)
        {
            Debug.WriteLine("Скачано " + album.Count);
            currentAlbum = album;

            for (int i=0; i < album.Count; i++)
            {
                if (album[i].pid.Equals(currentInfo.pid))
                {
                    currentIndex = i;
                    break;
                }
            }

            SetInformation();
        }

        async void Blur(bool fade = false)
        {
            StorageFile file = await StorageFile.GetFileFromPathAsync(currentInfo.path);

            using (var stream = await file.OpenAsync(FileAccessMode.Read))
            {
                var device = new CanvasDevice();
                var bitmap = await CanvasBitmap.LoadAsync(device, stream);
                var renderer = new CanvasRenderTarget(device, bitmap.SizeInPixels.Width, bitmap.SizeInPixels.Height, bitmap.Dpi);

                using (var ds = renderer.CreateDrawingSession())
                {
                    var blur = new GaussianBlurEffect();
                    blur.BlurAmount = 8.0f;
                    blur.BorderMode = EffectBorderMode.Hard;
                    blur.Optimization = EffectOptimization.Quality;
                    blur.Source = bitmap;
                    ds.DrawImage(blur);
                }

                var saveFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(currentInfo.pid+".png", CreationCollisionOption.ReplaceExisting);

                using (var outStream = await saveFile.OpenAsync(FileAccessMode.ReadWrite))
                {
                    await renderer.SaveAsync(outStream, CanvasBitmapFileFormat.Png);

                    if (fade)
                    {
                        image.Visibility = Visibility.Collapsed;
                        imageBlur.Visibility = Visibility.Collapsed;

                        image.Opacity = 0;
                        imageBlur.Opacity = 0;
                    }

                    this.imageBlur.Source = new BitmapImage(new Uri(ApplicationData.Current.LocalFolder.Path + "\\" + currentInfo.pid + ".png"));
                    this.image.Source = new BitmapImage(new Uri(currentInfo.path));

                    if (fade)
                    {
                        image.Visibility = Visibility.Visible;
                        imageBlur.Visibility = Visibility.Visible;
                    }

                    Debug.WriteLine(image.Opacity);
                }    
            }
        }

        private void ButtonLeft_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (currentAlbum == null)
                return;

            if (currentIndex >= 1)
                currentIndex = (currentIndex - 1);
            else
                currentIndex = currentAlbum.Count - 1;
            Debug.WriteLine(currentIndex);
            RequestCurrentID();
        }

        private void ButtonRight_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (currentAlbum == null)
                return;

            currentIndex = (currentIndex + 1) % currentAlbum.Count;
            Debug.WriteLine(currentIndex);
            RequestCurrentID();
        }

        void RequestCurrentID()
        {
            SetInformation();
            ShowProgress();
            VKAPI.instance.DownloadFullSizeImage(currentAlbum[currentIndex], ImageDownloaded);

        }

        void SetInformation()
        {
            counter.Text = (currentIndex + 1) + " из " + currentAlbum.Count;
            likes.Text = currentAlbum[currentIndex].likes.ToString();

            if (currentAlbum[currentIndex].text != "")
            {
                description.Text = currentAlbum[currentIndex].text;
                descriptionBorder.Visibility = Visibility.Visible;
            }
            else
            {
                description.Text = "";
                descriptionBorder.Visibility = Visibility.Collapsed;
            }

            //Debug.WriteLine(currentAlbum[currentIndex].fio);
            //fio.Text = currentAlbum[currentIndex].fio;
        }

    public void ImageDownloaded(ImageRequest request, ImageInfo info)
    {
        if (request.isSuccess)
        {
                currentInfo = info;
                firstOpen = false;
                currentInfo = info;
                Fade(imageBlur, "Opacity", 1, 0, 0.5f, FadeOutCompleted);
                fio.Content = info.fio;

                if (info.owner_id == VKAPI.instance.userId.ToString())
                {
                    Settings.IsEnabled = true;
                }
                else
                {
                    Settings.IsEnabled = false;
                }
        }
     }

        private void FadeOutCompleted(object sender, object e)
        {
            Fade(image, "Opacity", 1, 0, 0.5f, FadeCompleted);
        }

        private void FadeCompleted(object sender, object e)
        {
            Blur(true);
        }

        private void FadeOriginalCompleted(object sender, object e)
        {
            Fade(imageBlur, "Opacity", 0, 1, 0.5f);
        }

        private void imageBlur_ImageOpened(object sender, RoutedEventArgs e)
        {
            if (!firstOpen)
            {
                HideProgress();
                Fade(image, "Opacity", 0, 1, 0.5f, FadeOriginalCompleted);
            }
        }

        public static void Fade(DependencyObject target, string animationType, double from, double to, double time = 1, EventHandler<object> callback = null)
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.AutoReverse = false;
            animation.Completed += callback;

            TimeSpan timeSpan = TimeSpan.FromSeconds(time);
            animation.Duration = timeSpan;

            Storyboard storyBoard = new Storyboard();
            storyBoard.Children.Add(animation);

            Storyboard.SetTargetProperty(animation, animationType);

            Storyboard.SetTarget(storyBoard, target);

            storyBoard.Begin();
        }

        private async void Settings_Click(object sender, RoutedEventArgs e)
        {
            var result = await editPhotoDialog.ShowAsync();
        }

        string descriptionLabel = "";
        private void editPhotoSave_Click(object sender, RoutedEventArgs e)
        {
           descriptionLabel = photoDescriptionEdit.Text;
           VKAPI.instance.EditPhoto(currentAlbum[currentIndex].pid, descriptionLabel, EditPhotoResult);
           editPhotoDialog.Hide();
        }

        private void editPhotoCancel_Click(object sender, RoutedEventArgs e)
        {
            editPhotoDialog.Hide();
        }

        private void deletePhoto_Click(object sender, RoutedEventArgs e)
        {
            VKAPI.instance.DeletePhoto(currentAlbum[currentIndex].pid, PhotoDeleted);
        }

        public void PhotoDeleted(string json)
        {
            VKAPI.instance.GetAlbumsCount();
            ApplicationWindow.instance.OpenAlbums();
            editPhotoDialog.Hide();
        }

        public void EditPhotoResult(string json)
        {
            Debug.WriteLine(json);
            currentAlbum[currentIndex].text = descriptionLabel;
            description.Text = descriptionLabel;
            photoDescriptionEdit.Text = "";
            SetInformation();
        }

        private void fio_Click(object sender, RoutedEventArgs e)
        {
            Windows.System.Launcher.LaunchUriAsync(new Uri("http://www.vk.com/id"+currentInfo.owner_id));
        }
    }

}
