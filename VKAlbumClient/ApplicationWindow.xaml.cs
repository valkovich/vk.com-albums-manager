﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace VKAlbumClient
{
    public class Scenario
    {
        public string Title { get; set; }
        public Type ClassType { get; set; }
    }

    sealed partial class ApplicationWindow : Page
    {
        static public ApplicationWindow instance { get; private set; }
        public List<Scenario> Scenarios = new List<Scenario>();


        public ApplicationWindow()
        {
            this.InitializeComponent();
            instance = this;

            var titleBar = ApplicationView.GetForCurrentView().TitleBar;
            titleBar.BackgroundColor = Colors.SteelBlue;
            titleBar.ForegroundColor = Colors.White;
            titleBar.ButtonBackgroundColor = Colors.SteelBlue;
            titleBar.ButtonForegroundColor = Colors.White;
            

            Go(typeof(MainPage));
        }


        public void OpenMain(object sender, RoutedEventArgs e)
        {
            Go(typeof(MainPage));
        }

        public void OpenAlbums(object sender, RoutedEventArgs e)
        {
            OpenAlbums();
        }

        public void OpenAlbums()
        {
            Go(typeof(Albums));
        }

        private void Go(Type classType)
        {
            ScenarioFrame.Navigate(classType, new Windows.UI.Xaml.Media.Animation.PopupThemeTransition());
        }

        private void mapButton_Click(object sender, RoutedEventArgs e)
        {
            Go(typeof(Map));
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void menu_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            int choose = menu.SelectedIndex;

            switch (choose)
            {
                case 0: Go(typeof(MainPage)); break;
                case 1: Go(typeof(Albums)); break;
                case 2: Go(typeof(Map)); break;
            }
        }
    }

}

