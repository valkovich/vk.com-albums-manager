﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Networking.BackgroundTransfer;
using Windows.Security.Authentication.Web;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace VKAlbumClient
{
    public class User
    {
        public string name = "";
        public string photoUri = Windows.ApplicationModel.Package.Current.InstalledLocation.Path + "\\Assets\\Avatar.png";
    }

    public class Comment
    {
        public string cid { get; set; }
        public string from_id { get; set; }
        public string date { get; set; }
        public string message { get; set; }
        public string pid { get; set; }
        public string ownerPhotoPath { get; set; }


        public void SetMessage(string comment)
        {
            comment = comment.Replace("<br>", " ");

            if (comment.Contains("[id"))
            {
                int line = comment.IndexOf("|") + 1;
                comment = comment.Substring(line);
            }

            comment = comment.Replace("[", " ");
            comment = comment.Replace("]", " ");

            this.message = comment;
        }
    }

    public class ImageRequest
    {
        public bool isSuccess = false;
        public string filePath = String.Empty;

        public ImageRequest(string filePath)
        {
            this.filePath = filePath;
            this.isSuccess = true;
        }

        public ImageRequest()
        {
            this.isSuccess = false;
        }
    }

    public class ImageInfo
    {
        public string pid { get; set; }
        public string aid { get; set; }
        public string owner_id { get; set; }
        public string path { get; set; }
        public string text { get; set; }
        public int likes { get; set; }
        public string latCord { get; set; }
        public string longCord { get; set; }
        public string src { get; set; }
        public string likesText { get; set; }
        public string json { get; set; }
        public int jsonId;

        public string fio { get; set; }


        public void SetFio(string name, string sername)
        {
            fio = name + " " + sername;
        }

    public List<Comment> comments { get; set; }

        public ImageInfo(string pid, string aid, string owner_id, string path)
        {
            this.path = path;
            this.pid = pid;
            this.owner_id = owner_id;
            this.aid = aid;
            text = "";
        }
        public Geopoint location { get; set; }

        public void SetLocation(double lat, double longt)
        {
            BasicGeoposition gp = new BasicGeoposition();
            gp.Latitude = lat;
            gp.Longitude = longt;
            Geopoint geo = new Geopoint(gp);
            location = geo;
        }

        public void SetLocation(string lat, string longt)
        {
            lat = lat.Replace(".", ",");
            longt = longt.Replace(".", ",");

            BasicGeoposition gp = new BasicGeoposition();
            gp.Latitude = double.Parse(lat);
            gp.Longitude = double.Parse(longt);
            Geopoint geo = new Geopoint(gp);
            location = geo;
        }

        public void SetMessage(string comment)
        {
            comment = comment.Replace("<br>", " ");

            if (comment.Contains("[id"))
            {
                int line = comment.IndexOf("|") + 1;
                comment = comment.Substring(line);
            }

            comment = comment.Replace("[", " ");
            comment = comment.Replace("]", " ");

            this.text = comment;
        }

        public ImageInfo()
        {
        }

        public string OneLineSummary
        {
            get
            {
                return path;
            }
        }
    }

    public class AlbumsInfo
    {
        public string name { get; set; }
        public ImageInfo avatar { get; set; }
        public string aid;
        public string owner_id { get; set; }

        public string description { get; set; }
        public string size { get; set; }
        public Windows.UI.Xaml.Visibility privacy { get; set; }
        public AlbumsInfo(ImageInfo data, string title)
        {
            name = title;
            avatar = data;
        }
    }



    public class VKAPI
    {
        static public VKAPI instance { get; private set; }

        public int userId = 0;
        private string accessToken = "";
        static public User user = new User();
        public int photosCount = 0;

        public List<ImageInfo> mostLikeble = new List<ImageInfo>();
        public List<ImageInfo> mostCommentable = new List<ImageInfo>();

        public VKAPI()
        {
            instance = this;
        }

        private ObservableCollection<ImageInfo> previewImages = new ObservableCollection<ImageInfo>();

        public ObservableCollection<ImageInfo> PreviewImages
        {
            get { return previewImages; }
        }

        private ObservableCollection<AlbumsInfo> albums = new ObservableCollection<AlbumsInfo>();

        public ObservableCollection<AlbumsInfo> AlbumsPreview
        {
            get { return albums; }
        }

        private ObservableCollection<ImageInfo> likebleSort = new ObservableCollection<ImageInfo>();

        public ObservableCollection<ImageInfo> LikebleSort
        {
            get { return likebleSort; }
        }

        private ObservableCollection<ImageInfo> imagesWithComments = new ObservableCollection<ImageInfo>();

        public ObservableCollection<ImageInfo> ImagesWithComments
        {
            get { return imagesWithComments; }
        }

        public async void OAuthVk()
        {
            try
            {
                const string vkUri = "https://oauth.vk.com/authorize?client_id=5128225&scope=4&" + "redirect_uri=http://oauth.vk.com/blank.html&display=touch&response_type=token";
                Uri requestUri = new Uri(vkUri);
                Uri callbackUri = new Uri("http://oauth.vk.com/blank.html");
                WebAuthenticationResult result = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, requestUri, callbackUri);

                switch (result.ResponseStatus)
                {
                    case WebAuthenticationStatus.ErrorHttp:
                        MessageDialog dialogError = new MessageDialog("Не удалось открыть страницу сервиса\n" + "Попробуйте войти в приложения позже!", "Ошибка");
                        dialogError.ShowAsync();
                        break;

                    case WebAuthenticationStatus.Success:
                        string responseString = result.ResponseData;
                        Debug.WriteLine("Авторизация прошла успешно");
                        ParseToken(responseString);
                        break;
                }
            }
            catch
            {
                MessageDialog m = new MessageDialog("Ошибка соединения", "Ошибка");

                m.Commands.Add(new UICommand(("Переподключиться"), (command) =>
                 {
                     OAuthVk();
                 }));

                m.Commands.Add(new UICommand(("Выйти"), (command) =>
                {
                    Application.Current.Exit();
                }));

                var result = m.ShowAsync();
            }
        }


        void ParseToken(string responseString)
        {
            char[] separators = { '=', '&' };
            string[] responseContent = responseString.Split(separators);
            accessToken = responseContent[1];
            userId = Int32.Parse(responseContent[5]);
            App.instance.OpenMain();
            GetUser();
            GetAllComments();
        }

        string Request(string method, string param)
        {
            string request = "https://api.vk.com/method/" + method + "?" + param + "&access_token=" + accessToken;
            Debug.WriteLine(request);
            return request;
        }

        async void GetUser()
        {
            string requestURL = Request("users.get", "fields=photo_400_orig,photo_id");
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();

            SetName(jsonResponse);
            SetPhoto(jsonResponse);
        }

        List<ImageInfo> allImagesEver = new List<ImageInfo>();

        public async void GetPreviewPhotos()
        {
            try
            {
                string requestURL = Request("photos.getAll", "owner_id=" + userId.ToString() + "&count=21&extended=1");
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();

                dynamic stuff = JObject.Parse(jsonResponse);

                previewImages.Clear();

                string count = stuff.response[0];

                int intCount = int.Parse(count);

                SetAllPhotosCount(count);

                int preCount = intCount;

                if (preCount >= 21)
                    preCount = 21;

                for (int i = 1; i <= preCount; i++)
                {
                    if (stuff.response[i] == null)
                        break;

                    JObject o = JObject.Parse(jsonResponse);
                    string pid = stuff.response[i].pid;
                    string aid = stuff.response[i].aid;
                    string src = stuff.response[i].src_big;
                    string text = stuff.response[i].text;

                    string likes = (o["response"][i]["likes"]["count"]).ToString();

                    ImageInfo info = new ImageInfo();
                    info.pid = pid;
                    info.aid = aid;
                    info.text = text;
                    info.src = src;
                    info.likes = int.Parse(likes);
                    info.likesText = "Нравится: " + likes;
                    info.owner_id = userId.ToString();
                    ImageDownloader d = new ImageDownloader(src, PreviewDownloaded, info);
                }

                MainPage.instance.ClosePreviewProgress();

                await Task.Delay(TimeSpan.FromSeconds(0.5f));


                int globalI = 0;

                for (int i = 0; i < intCount; i += 200)
                {
                    requestURL = Request("photos.getAll", "owner_id=" + userId.ToString() + "&count=200" + "&offset=" + i + "&extended=1");
                    client = new HttpClient();
                    client.MaxResponseContentBufferSize = 256000;
                    response = await client.GetAsync(requestURL);
                    response.EnsureSuccessStatusCode();
                    jsonResponse = await response.Content.ReadAsStringAsync();
                    stuff = JObject.Parse(jsonResponse);
                    JObject o = JObject.Parse(jsonResponse);
                    JArray array = stuff.response;

                    for (int j = 1; j <= array.Count - 1; j++)
                    {
                        if (globalI >= intCount)
                            break;

                        string pid = stuff.response[j].pid;
                        string aid = stuff.response[j].aid;
                        string src = stuff.response[j].src_big;
                        string text = stuff.response[j].text;

                        ImageInfo info = new ImageInfo();
                        info.pid = pid;
                        info.aid = aid;
                        info.text = text;
                        info.src = src;


                        if (o["response"][j]["likes"] != null)
                        {
                            string likes = (o["response"][j]["likes"]["count"]).ToString();
                            info.likes = int.Parse(likes);
                            info.likesText = "Нравится: " + likes;
                        }

                        info.owner_id = userId.ToString();
                        globalI++;
                        mostLikeble.Add(info);
                    }
                }
            }
            catch
            {
                await Task.Delay(TimeSpan.FromSeconds(1.5f));
                GetPreviewPhotos();
                return;
            }

            FindMostLikeble();
        }

        void FindMostLikeble()
        {
            List<ImageInfo> list = new List<ImageInfo>();
            var sortict = mostLikeble.OrderByDescending(x => x.likes);

            foreach (ImageInfo info in sortict)
                list.Add(info);

            int size = 40;
            if (list.Count < 40)
                size = list.Count;

            for (int i = 0; i < size; i++)
            {
                ImageDownloader d = new ImageDownloader(list[i].src, LikebleDownloaded, list[i]);
            }

            GetAlbumsCount();
        }

        void LikebleDownloaded(ImageRequest image, ImageInfo info)
        {
            if (image.isSuccess)
            {
                likebleSort.Add(info);
                MainPage.instance.SetMostPopular();
            }
        }

        public Dictionary<string, List<Comment>> comments = new Dictionary<string, List<Comment>>();
        List<Comment> allComments = new List<Comment>();
        public Dictionary<string, string> avatars = new Dictionary<string, string>();

        public async void GetAllComments()
        {
            string requestURL = Request("photos.getAllComments", "owner_id=" + userId.ToString() + "&count=1000&version=5.40");
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();

            dynamic stuff = JObject.Parse(jsonResponse);
            JArray array = stuff.response;
            int intCount = array.Count;


            for (int i = 1; i < intCount; i++)
            {
                string pid = stuff.response[i].pid;
                string cid = stuff.response[i].cid;
                string from_id = stuff.response[i].from_id;
                string date = stuff.response[i].date;
                string message = stuff.response[i].message;

                Comment comment = new Comment();
                comment.pid = pid;
                comment.cid = cid;
                comment.from_id = from_id;
                comment.date = date;
                comment.SetMessage(message);

                allComments.Add(comment);
            }

            foreach (Comment cm in allComments)
            {
                if (!comments.ContainsKey(cm.pid))
                {
                    comments.Add(cm.pid, new List<Comment>());
                    comments[cm.pid].Add(cm);
                }
                else
                {
                    comments[cm.pid].Add(cm);
                }
            }

            var ordered = comments.OrderByDescending(x => x.Value.Count);

            int commentPostCount = 0;

            int showCount = intCount;

            if (intCount > 10)
                showCount = 10;


            string avatarsRequest = "";

            foreach (Comment cm in allComments)
            {
                if (!avatars.ContainsKey(cm.from_id))
                {
                    avatars.Add(cm.from_id,"");
                    avatarsRequest += cm.from_id + ",";
                }
            }

                        Debug.WriteLine("COMMENTS: " + allComments.Count);


            if (comments.Count == 0)
                return;


            requestURL = Request("users.get", "&user_ids="+avatarsRequest+ "&fields=photo_50");
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            jsonResponse = await response.Content.ReadAsStringAsync();
            stuff = JObject.Parse(jsonResponse);

            JArray avaArray = stuff.response;


            for (int i = 0; i < avaArray.Count; i++)
            {
                string uid = stuff.response[i].uid;
                string avaPath = stuff.response[i].photo_50;
                avatars[uid] = avaPath;
            }

            string request = "";
            foreach (KeyValuePair<string, List<Comment>> elem in ordered)
            {
                if (commentPostCount >= showCount)
                    break;

                request += userId.ToString() + "_" + elem.Key+",";
                commentPostCount++;
            }

            request = request.Remove(request.Length - 1);

            requestURL = Request("photos.getById", "&photo_sizes=1&photos=" + request);
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            jsonResponse = await response.Content.ReadAsStringAsync();
            stuff = JObject.Parse(jsonResponse);
            commentPostCount = 0;
            foreach (KeyValuePair<string, List<Comment>> elem in ordered)
            {
                if (commentPostCount >= showCount)
                    break;

                elem.Value.Reverse();

                JArray jarray = stuff.response[commentPostCount].sizes;

                int max = 0;
                int maxid = 0;
                for (int i = 0; i < jarray.Count; i++)
                {
                    string widthS = stuff.response[commentPostCount].sizes[i].width;
                    int width = int.Parse(widthS);

                    if (width > max)
                    {
                        maxid = i;
                        max = width;
                    }
                }

                string src = stuff.response[commentPostCount].sizes[maxid].src;
                string aid = stuff.response[commentPostCount].aid;
                string text = stuff.response[commentPostCount].text;

                string lat = "", longCord = "";

                if (jsonResponse.Contains("lat"))
                    lat = stuff.response[commentPostCount].lat;

                if (jsonResponse.Contains("long"))
                {
                    string longCordTrim = jsonResponse.Substring(jsonResponse.IndexOf("long"));
                    string[] elems = longCordTrim.Split(',');
                    string cords = elems[0].Replace("long", "");
                    cords = cords.Substring(2);
                    longCord = cords;
                }

                ImageInfo info = new ImageInfo();
                info.pid = elem.Key;
                info.comments = elem.Value;
                info.aid = aid;
                info.json = jsonResponse;
                info.longCord = longCord;
                info.latCord = lat;
                info.src = src;
                info.text = text;
                info.owner_id = userId.ToString();

                foreach (Comment com in info.comments)
                {
                    com.ownerPhotoPath = avatars[com.from_id];
                }

                imagesWithComments.Add(info);

                commentPostCount++;
            }

            MainPage.instance.SetMostCommentable();
        }



        void SetAllPhotosCount(string count)
        {
            photosCount = int.Parse(count);
            MainPage.instance.SetAllPhotosCount();
        }

        void PreviewDownloaded(ImageRequest image, ImageInfo info)
        {
            if (image.isSuccess)
            {
                previewImages.Add(info);
                MainPage.instance.SetAllPhotosPreview();
            }
        }



        public async void GetAlbumsCount()
        {
            int albumsCount = 0;
            try
            {
                string requestURL = Request("photos.getAlbumsCount", "user_id=" + userId.ToString());
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();

                dynamic stuff = JObject.Parse(jsonResponse);
                string count = stuff.response;
                albumsCount = int.Parse(count);
            }
            catch
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                GetAlbumsCount();
                return;
            }

            GetPreviewAlbums(albumsCount);
        }



        async void GetPreviewAlbums(int count)
        {
            string requestURL = Request("photos.getAlbums", "need_covers=1&photo_sizes=1");
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            dynamic stuff = JObject.Parse(jsonResponse);

            albums.Clear();

            try
            {

                for (int i = 0; i < count; i++)
                {
                    string src = stuff.response[i].sizes[2].src;
                    string pid = "";

                    ImageInfo info = new ImageInfo(pid, "", "", src);
                    info.jsonId = i;
                    info.json = jsonResponse;
                    info.owner_id = userId.ToString();

                    ImageDownloader d = new ImageDownloader(src, PreviewAlbumsDownloaded, info);
                }
            }
            catch
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5f));
                GetPreviewAlbums(count);
            }
        }

        async public void DownloadFullSizeImage(ImageInfo imInfo, Action<ImageRequest, ImageInfo> method)
        {
            try
            {
                string requestURL = Request("photos.getById", "photos=" + imInfo.owner_id + "_" + imInfo.pid + "&extended=1&photo_sizes=1");
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();
                dynamic stuff = JObject.Parse(jsonResponse);

                Debug.WriteLine(jsonResponse);

                JArray array = stuff.response[0].sizes;

                int max = 0;
                int maxid = 0;
                for (int i = 0; i < array.Count; i++)
                {
                    string widthS = stuff.response[0].sizes[i].width;
                    int width = int.Parse(widthS);

                    if (width > max)
                    {
                        maxid = i;
                        max = width;
                    }
                }

                string src = stuff.response[0].sizes[maxid].src;
                string aid = stuff.response[0].aid;
                string likes = stuff.response[0].likes.user_likes;

                string lat = "", longCord = "";

                if (jsonResponse.Contains("lat"))
                    lat = stuff.response[0].lat;

                if (jsonResponse.Contains("long"))
                {
                    string longCordTrim = jsonResponse.Substring(jsonResponse.IndexOf("long"));
                    string[] elems = longCordTrim.Split(',');
                    string cords = elems[0].Replace("long", "");
                    cords = cords.Substring(2);
                    longCord = cords;
                }


                requestURL = Request("users.get", "&user_ids=" + imInfo.owner_id);
                client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                jsonResponse = await response.Content.ReadAsStringAsync();
                stuff = JObject.Parse(jsonResponse);


                string first_name = stuff.response[0].first_name;
                string last_name = stuff.response[0].last_name;

                ImageInfo info = new ImageInfo(imInfo.pid, aid, "", src);
                info.aid = aid;
                info.json = jsonResponse;
                info.likes = int.Parse(likes);
                info.longCord = longCord;
                info.latCord = lat;
                info.SetFio(first_name, last_name);
                info.owner_id = imInfo.owner_id;
                Debug.WriteLine("VK " + info.fio);

                ImageDownloader d = new ImageDownloader(src, method, info);
            }

            catch
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5f));
                DownloadFullSizeImage(imInfo, method);
                return;
            }
        }


        void PreviewAlbumsDownloaded(ImageRequest image, ImageInfo info)
        {
            if (image.isSuccess)
            {
                dynamic stuff = JObject.Parse(info.json);
                string title = stuff.response[info.jsonId].title;
                string aid = stuff.response[info.jsonId].aid;
                string sieze = stuff.response[info.jsonId].size;
                string decription = stuff.response[info.jsonId].description;
                string privacy = stuff.response[info.jsonId].privacy;
                string owner_id = stuff.response[info.jsonId].owner_id;

                AlbumsInfo aInfo = new AlbumsInfo(new ImageInfo("", aid, "", info.path), title);
                aInfo.aid = aid;
                aInfo.owner_id = owner_id;
                aInfo.size = sieze;
                aInfo.description = decription;
                aInfo.privacy = int.Parse(privacy) != 0 ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
                albums.Add(aInfo);
                MainPage.instance.SetAllAlbumsPrevies();
            }
        }


        async public void DownloadAlbumFormCurrentImage(AlbumsInfo aInfo, Action<List<ImageInfo>> method)
        {
            List<ImageInfo> currentAlbum = new List<ImageInfo>();

            try
            {
                string requestURL = Request("photos.get", "owner_id=" + aInfo.owner_id + "&album_id=" + aInfo.aid + "&photo_sizes=1&count=1000&extended=1");
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 1000000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();

                dynamic stuff = JObject.Parse(jsonResponse);

                JArray array = stuff.response;

                Debug.WriteLine(jsonResponse);


                for (int i = 0; i < array.Count; i++)
                {
                    JArray sizes = stuff.response[i].sizes;


                    int max = 0;
                    int maxid = 0;
                    for (int j = 0; j < sizes.Count; j++)
                    {
                        if (stuff.response[i].sizes[j] != null)
                        {

                            string widthS = stuff.response[i].sizes[j].width;


                            int width = int.Parse(widthS);

                            if (width > max)
                            {
                                maxid = j;
                                max = width;
                            }
                        }
                    }

                    string src = stuff.response[i].sizes[maxid].src;
                    string pid = stuff.response[i].pid;
                    string text = stuff.response[i].text;
                    string likes = stuff.response[i].likes.count;

                    string lat = "", longCord = "";

                    if (jsonResponse.Contains("lat"))
                        lat = stuff.response[0].lat;

                    if (jsonResponse.Contains("long"))
                    {
                        string longCordTrim = jsonResponse.Substring(jsonResponse.IndexOf("long"));
                        string[] elems = longCordTrim.Split(',');
                        string cords = elems[0].Replace("long", "");
                        cords = cords.Substring(2);
                        longCord = cords;
                    }

                    ImageInfo info = new ImageInfo(pid, aInfo.aid, "", src);
                    info.json = jsonResponse;
                    info.likes = int.Parse(likes);
                    info.longCord = longCord;
                    info.latCord = lat;
                    info.text = text;
                    info.owner_id = aInfo.owner_id;
                    currentAlbum.Add(info);
                }
            }
            catch
            {
                DownloadAlbumFormCurrentImage(aInfo, method);
                await Task.Delay(TimeSpan.FromSeconds(1.5f));
                return;
            }

            method(currentAlbum);
        }

        void SetName(string json)
        {
            dynamic stuff = JObject.Parse(json);
            string name = stuff.response[0].first_name + " " + stuff.response[0].last_name;
            user.name = name;
            MainPage.instance.SetName();
        }

        async void SetPhoto(string json)
        {
            //try
            //{

            dynamic stuff = JObject.Parse(json);
            string photo_400_orig = stuff.response[0].photo_400_orig;
            user.photoUri = photo_400_orig;


            string photoid = stuff.response[0].photo_id;

            if (photoid != null)
            {
                string[] photoids = photoid.Split('_');
                photoid = photoids[photoids.Length - 1];


                string requestURL = Request("photos.getById", "&photos=" + userId.ToString() + "_" + photoid);
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();
                stuff = JObject.Parse(jsonResponse);

                string aid = stuff.response[0].aid;

                ImageInfo info = new ImageInfo("", "", "", user.photoUri);
                info.pid = photoid;
                info.owner_id = userId.ToString();
                info.aid = aid;
                ImageDownloader d = new ImageDownloader(user.photoUri, AvatarDownloaded, info);
            }

            await Task.Delay(TimeSpan.FromSeconds(1.5f));
            GetPreviewPhotos();
        }

        void AvatarDownloaded(ImageRequest image, ImageInfo info)
        {
            avatarInfo = info;
            MainPage.instance.SetAvatar();
        }

        public ImageInfo avatarInfo;

        public async void CreateAlbum(string name, string description, string privacy, Action<string> method)
        {
            string requestURL = Request("photos.createAlbum", "title=" + name + "&description=" + description + "&privacy=" + privacy);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            method(jsonResponse);
        }

        public async void EditAlbum(string aid, string name, string description, string privacy, Action<string> method)
        {
            string requestURL = Request("photos.editAlbum", "album_id=" + aid + "&title=" + name + "&description=" + description + "&privacy=" + privacy);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            method(jsonResponse);
        }

        public async void EditPhoto(string pid, string description, Action<string> method)
        {
            string requestURL = Request("photos.edit", "photo_id=" + pid + "&caption=" + description);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            method(jsonResponse);
        }

        public static string Translit(string str)
        {
            string[] lat_up = { "A", "B", "V", "G", "D", "E", "Yo", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "Ts", "Ch", "Sh", "Shch", "\"", "Y", "'", "E", "Yu", "Ya" };
            string[] lat_low = { "a", "b", "v", "g", "d", "e", "yo", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "ts", "ch", "sh", "shch", "\"", "y", "'", "e", "yu", "ya" };
            string[] rus_up = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я" };
            string[] rus_low = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я" };
            for (int i = 0; i <= 32; i++)
            {
                str = str.Replace(rus_up[i], lat_up[i]);
                str = str.Replace(rus_low[i], lat_low[i]);
            }
            return str;
        }

        public async void UploadPhoto(string album, StorageFile file, string description, Action<string> method)
        {
            string requestURL = Request("photos.getUploadServer", "album_id=" + album);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();

            dynamic stuff = JObject.Parse(jsonResponse);
            string uploadUrl = stuff.response.upload_url;
            Uri url = new Uri(uploadUrl);

            if (Regex.IsMatch(file.Name, @"\p{IsCyrillic}"))
            {
                await file.RenameAsync(Translit(file.Name));
            }

            byte[] fileBytes = null;
            using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (DataReader reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }

            HttpClient client1 = new HttpClient();
            MultipartContent content = new System.Net.Http.MultipartFormDataContent();
            var file1 = new ByteArrayContent(fileBytes);
            file1.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
            {
                Name = "file1",
                FileName = file.Name
            };

            content.Add(file1);
            HttpResponseMessage response1 = await client1.PostAsync(url, content);
            response1.EnsureSuccessStatusCode();
            var arr = await response1.Content.ReadAsByteArrayAsync();
            string data = BytesToStringConverted(arr);


            stuff = JObject.Parse(data);
            string server = stuff.server;
            string photos_list = stuff.photos_list;
            string hash = stuff.hash;
       

            requestURL = Request("photos.save", "album_id=" + album + "&server=" + server + "&photos_list=" + photos_list + "&hash=" + hash + "&caption=" + description);
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            jsonResponse = await response.Content.ReadAsStringAsync();


            stuff = JObject.Parse(jsonResponse);
            string aid = stuff.response[0].aid;

            method(aid);


            method(jsonResponse);
        }

        static string BytesToStringConverted(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        private ObservableCollection<ImageInfo> mapImages = new ObservableCollection<ImageInfo>();

        public ObservableCollection<ImageInfo> MapImages
        {
            get { return mapImages; }
        }

        async public void GetMapPhoto(string latin, string longin, Action method)
        {
            try
            {
                string requestURL = Request("photos.search", "lat=" + latin + "&long=" + longin + "&radius=6000&count=300");
                HttpClient client = new HttpClient();
                client.MaxResponseContentBufferSize = 1000000;
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                string jsonResponse = await response.Content.ReadAsStringAsync();
                dynamic stuff = JObject.Parse(jsonResponse);

                JArray array = stuff.response;

                int size = array.Count;
                hideMapCount = size;
                hideMapCountCurrent = 0;
                for (int i = 1; i < size; i++)
                {
                    string pid = stuff.response[i].pid;
                    string aid = stuff.response[i].aid;
                    string src = stuff.response[i].src;
                    string owner_id = stuff.response[i].owner_id;
                    string text = stuff.response[i].text;
                    string lat = stuff.response[i].lat;
                    string longc = stuff["response"][i]["long"];

                    ImageInfo info = new ImageInfo();
                    info.pid = pid;
                    info.aid = aid;
                    info.src = src;
                    info.owner_id = owner_id;
                    info.SetMessage(text);
                    info.SetLocation(lat, longc);

                    if (!owner_id.Contains("-"))
                        mapImages.Add(info);

                    ImageDownloader d = new ImageDownloader(info.src, MapDownloadedItem, info);

                }
            }
            catch
            {
                await Task.Delay(TimeSpan.FromSeconds(1f));
                GetMapPhoto(latin, longin, method);
                return;
            }

            method();
        }

        int hideMapCount = 0;
        int hideMapCountCurrent = 0;

        void MapDownloadedItem(ImageRequest request, ImageInfo info)
        {
            hideMapCountCurrent++;
            Map.instance.Finished();

            if (hideMapCount-1 == hideMapCountCurrent)
            {
                Map.instance.HideProgress();
            }
        }

        async public void DeleteAlbum(string aid, Action<string> method)
        {
            string requestURL = Request("photos.deleteAlbum", "album_id=" + aid);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            method(jsonResponse);
        }

        async public void DeletePhoto(string pid, Action<string> method)
        {
            string requestURL = Request("photos.delete", "photo_id=" + pid);
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            HttpResponseMessage response = await client.GetAsync(requestURL);
            response.EnsureSuccessStatusCode();
            string jsonResponse = await response.Content.ReadAsStringAsync();
            method(jsonResponse);
        }
    }

    public class ImageDownloader
    {
        Action<ImageRequest, ImageInfo> callback = delegate { };
        ImageInfo info;

        public ImageDownloader(string uri, Action<ImageRequest, ImageInfo> method, ImageInfo info)
        {
            this.callback = method;
            this.info = info;

            Download(uri);
        }

        async void Download(string uri)
        {
            if (CheckCacheImage(ClearLink(uri)))
            {
                string path = ApplicationData.Current.LocalFolder.Path + "\\" + ClearLink(uri);
                info.path = path;
                callback(new ImageRequest(path), info);
                return;
            }

            Uri source;

            if (!Uri.TryCreate(uri, UriKind.Absolute, out source))
            {
                callback(new ImageRequest(), info);
                return;
            }

            StorageFile destinationFile = null; ;
            try
            {
                destinationFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(ClearLink(uri), CreationCollisionOption.ReplaceExisting);
            }
            catch (FileNotFoundException ex)
            {
                callback(new ImageRequest(), info);
                return;
            }

            BackgroundDownloader downloader = new BackgroundDownloader();
            DownloadOperation download = downloader.CreateDownload(source, destinationFile);
            await HandleDownloadAsync(download, true, uri, info);
        }

        private async Task HandleDownloadAsync(DownloadOperation download, bool start, string uri, ImageInfo info)
        {
            try
            {
                if (start)
                {
                    await download.StartAsync();
                }

                ResponseInformation response = download.GetResponseInformation();

                if (CheckCacheImage(ClearLink(uri)))
                {
                    string path = ApplicationData.Current.LocalFolder.Path + "\\" + ClearLink(uri);
                    info.path = path;
                    callback(new ImageRequest(path), info);
                }
            }
            catch
            {
            }
        }

        string ClearLink(string link)
        {
            string newlink = link.Replace("/", "");
            newlink = newlink.Replace("\\", "");
            newlink = newlink.Replace(",", "");
            newlink = newlink.Replace(":", "");
            return newlink;
        }

        async void SaveToStorage(string image, byte[] imageBuffer)
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile file = await storageFolder.GetFileAsync(image);
            await Windows.Storage.FileIO.WriteBytesAsync(file, imageBuffer);
        }

        private async Task<Uri> GetLocalImageAsync(string internetUri, string uniqueName)
        {
            if (string.IsNullOrEmpty(internetUri))
            {
                return null;
            }

            using (var response = await HttpWebRequest.CreateHttp(internetUri).GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    var desiredName = string.Format("{0}.jpg", uniqueName);
                    var file = await ApplicationData.Current.LocalFolder.CreateFileAsync(desiredName, CreationCollisionOption.ReplaceExisting);

                    using (var filestream = await file.OpenStreamForWriteAsync())
                    {
                        await stream.CopyToAsync(filestream);
                        return new Uri(string.Format("ms-appdata:///local/{0}.jpg", uniqueName), UriKind.Absolute);
                    }
                }
            }
        }

        bool CheckCacheImage(string name)
        {
            string path = ApplicationData.Current.LocalFolder.Path + "\\" + name;
            return File.Exists(path);
        }
    }
}
