﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace VKAlbumClient
{
    public sealed partial class AlbumViewer : Page
    {
        static public AlbumViewer instance { get; private set; }

        private List<ImageInfo> previewImages = new List<ImageInfo>();

        string albumName = String.Empty;
        string aid = String.Empty;
        string album_name = String.Empty;
        string album_description = String.Empty;

        AlbumsInfo current;

        public List<ImageInfo> PreviewImages
        {
            get { return previewImages; }
        }

        public AlbumViewer()
        {
            this.InitializeComponent();
            instance = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            AlbumsInfo info = (AlbumsInfo)e.Parameter;
            current = info;
            albumName = info.name;
            aid = info.aid;
            album_description = info.description;
            AlbumDescription.Text = album_description;

            albumNameEdit.Text = albumName;
            albumDescriptionEdit.Text = album_description;

            VKAPI.instance.DownloadAlbumFormCurrentImage(info, AlbumDownloaded);
            base.OnNavigatedTo(e);
        }

        public void AlbumDownloaded(List<ImageInfo> album)
        {
            Debug.WriteLine("Скачано " + album.Count);
            previewImages = album;
            AlbumsGrid.DataContext = previewImages;
            AlbumName.Text = albumName;
        }

        private void AlbumsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ImageInfo Purchase_Invoice = (ImageInfo)AlbumsGrid.SelectedItems[0];

            this.Frame.Navigate(
                        typeof(ImageViewer),
                        Purchase_Invoice,
                        new Windows.UI.Xaml.Media.Animation.DrillInNavigationTransitionInfo());
        }

        StorageFile lastFile;

        private async void attachPhoto_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            lastFile = null;
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");
            StorageFile file = await openPicker.PickSingleFileAsync();

            if (file != null)
            {
                photoStatus.Text = (file.Name.Length <= 15) ? file.Name : file.Name.Remove(15);
                lastFile = file;
                uploadphoto.IsEnabled = true;
            }
            else
            {
                photoStatus.Text = "Прикрепите фотографию";
                uploadphoto.IsEnabled = false;
            }
        }

        private async void NewPhoto_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var result = await addPhotoDialog.ShowAsync();
            uploadphoto.IsEnabled = false;
            lastFile = null; ;
            photoStatus.Text = "Прикрепите фото";
        }

        private void uploadphoto_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (lastFile!= null)
            {
                uploadBar.IsEnabled = true;
                uploadBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
                VKAPI.instance.UploadPhoto(aid, lastFile, photoDescription.Text, PhotoUploadResult);
                uploadphoto.IsEnabled = false;
            }
        }

        public void PhotoUploadResult(string data)
        {
            Debug.WriteLine("PhotoUploadResult " + data);

            VKAPI.instance.DownloadAlbumFormCurrentImage(current, AlbumDownloaded);
            VKAPI.instance.GetPreviewPhotos();
            VKAPI.instance.GetAlbumsCount();

            lastFile = null;
            photoDescription.Text = "";
            addPhotoDialog.Hide();
            uploadBar.IsEnabled = false;
            uploadBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private void cancelUploadPhoto_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            lastFile = null;
            photoDescription.Text = "" ;
            addPhotoDialog.Hide();
            uploadBar.IsEnabled = false;
            uploadBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void addPhotoDialog_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            uploadphoto.IsEnabled = false;
        }

        private async void EditAlbum_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var result = await editAlbumDialog.ShowAsync();
            albumNameEdit.Text = albumName;
            albumDescriptionEdit.Text = album_description;
        }

        private void editAlbumCancel_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            editAlbumDialog.Hide();
        }

        private void editAlbumSave_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (albumNameEdit.Text.Length >= 2)
            {
                string name = albumNameEdit.Text;
                string description = albumDescriptionEdit.Text;
                int privacyID = albumsPrivacyEdit.SelectedIndex;
                album_name = name;
                album_description = description;

                string privacy = String.Empty;

                switch (privacyID)
                {
                    case 0: privacy = "0"; break;
                    case 1: privacy = "1"; break;
                    case 2: privacy = "3"; break;
                    default: privacy = "3"; break;
                }
                VKAPI.instance.EditAlbum(aid, name, description, privacy, AlbumEditCallback);

                editAlbumDialog.Hide();
            }
            else
            {
                albumNameEdit.Text = "Название";
            }
        }


        public void AlbumEditCallback(string json)
        {
            VKAPI.instance.GetAlbumsCount();
            AlbumName.Text = album_name;
            AlbumDescription.Text = album_description;
            Debug.WriteLine(json);
        }

        private void deleteAlbum_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            VKAPI.instance.DeleteAlbum(aid, AlbumDeleted);
        }

        public void AlbumDeleted(string json)
        {
            ApplicationWindow.instance.OpenAlbums();
            VKAPI.instance.GetAlbumsCount();
            editAlbumDialog.Hide();
        }
    }
}
